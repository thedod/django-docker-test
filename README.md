Based on example at https://blog.bitsacm.in/django-on-docker/
(django+gunicorn+nginx+postgresql on docker)

### To install local dev server (sqlite3, and DEBUG):

* `virtualenv -p python3 venv`
* `. venv/bin/activate`
* `pip install -r djangoproject/requirements.txt`

#### To run local dev server
* First time in a shell, run `. venv/bin/activate`, and `. dev.env`
* `cd djangoproject`
* Perform _first time operations_ (see below)
* `./manage.py runserver` and browse to http://localhost:8000

### To deploy dockers locally

* You should copy `project.env.example` to `project.env` and edit it.
* `docker-compose up` would then run the server on http://localhost:8888
* `docker exec -it django /bin/bash` will let you perform _first time operations_ (see below)

### First time operations

Whether on local dev server or on a docker, you should do 2 things:

* `./manage.py createsuperuser` (so that you can manage the site via web)
* `./manage.py loaddata billboard/fixtures/initial-data.json` (Optional. Populates the db with a few items)

### Screenshots

![App](screenshots/django-test-app.png)

![Admin](screenshots/django-test-app-admin.png)
