from django.shortcuts import render

from .models import Item

def index(request):
    return render(request, 'billboard/index.html', {
        'title': 'My Recommendations',
        'items': Item.objects.all()})
