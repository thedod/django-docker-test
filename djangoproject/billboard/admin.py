from django.contrib import admin

from .models import Item

class ItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'link', 'modified']
    readonly_fields = ['modified']

admin.site.register(Item, ItemAdmin)

