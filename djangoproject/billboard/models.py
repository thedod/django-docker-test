from django.db import models
from django.utils import timezone

# Maybe one day we'd want to localize this
def _(s): return s

class Item(models.Model):
    title = models.CharField(_("Title"), max_length=64)
    link = models.URLField(_("link"), blank=True)
    description = models.TextField(_("Description"))
    modified = models.DateTimeField(_("Modified"), default=timezone.now)

    def save(self, *args, **kwargs):
        self.modified = timezone.now()
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')
        ordering = [ '-modified' ]


